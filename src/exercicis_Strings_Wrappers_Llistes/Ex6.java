package exercicis_Strings_Wrappers_Llistes;

public class Ex6 {

	public static void main(String[] args) {
		int total = 0;
		for (String numero : args) {
			int i = Integer.parseInt(numero);
			total+=i;
		}
		System.out.println("Total: "+total);
	}
}