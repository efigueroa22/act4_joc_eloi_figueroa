package exercicis_Strings_Wrappers_Llistes;
import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringBuilder cadena;
		
		do {
			System.out.println("Cadena:");
			cadena = new StringBuilder(sc.nextLine());
		} while(cadena.length()==0);
			
		System.out.println(cadena.reverse());
		
		sc.close();
	}
}