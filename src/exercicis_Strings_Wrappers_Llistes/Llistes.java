package exercicis_Strings_Wrappers_Llistes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Llistes {

	public static void main(String[] args) {
		ArrayList<String> telefons = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);
		int opcion, posicio;
		String telefon;
		
		do {
			System.out.println("Selecciona una opcio del menu");
			System.out.println("0. Sortir");
			System.out.println("1. Inserir un nou telefon a la llista");
			System.out.println("2. Imprimir la llista ordenada");
			System.out.println("3. Mostrar una posicio de la llista");
			System.out.println("4. Buscar si tenim un numero a la llista i mostrar-lo");
			System.out.println("5. Esborrar un numero de la llista");
			opcion = sc.nextInt();
			
			if(opcion == 0) {
				break;
			}
			else if(opcion == 1) {
				//demanar i validar el telèfon
				do {
					System.out.println("Telefon:");
					telefon = sc.next();
				}while(!telefon.matches("[6-7][0-9]{8}"));
				
				telefons.add(telefon);
				break;
			}
			else if(opcion == 2) {
				Collections.sort(telefons);
				//telefons.sort(null);
				System.out.println(telefons);
				break;
			}
			else if(opcion == 3) {
				do {
					System.out.println("Posicio:");
					posicio = sc.nextInt();
				}while(posicio < 0 || posicio > telefons.size()-1);
				
				System.out.println(telefons.get(posicio));
				//System.out.println(telefons.get(sc.nextInt()));
				break;
			}
			else if(opcion == 4) {
				//demanar y validar telèfon
				do {
					System.out.println("Telefon:");
					telefon = sc.next();
				}while(!telefon.matches("[6-7][0-9]{8}"));
				
				if(telefons.contains(telefon)) {
					System.out.println("Existeix");
				}
				else {
					System.out.println("No existeix");
				}
				break;
			}
			else if(opcion == 5) {
				//demanar i validar el telèfon
				do {
					System.out.println("Telefon:");
					telefon = sc.next();
				}while(!telefon.matches("[6-7][0-9]{8}"));
				
				if(telefons.remove(telefon)) {
					System.out.println("Borrat amb exit");
				}
				else {
					System.out.println("No existeix");
				}
				break;
			}
		} while(opcion != 0);
		
		System.out.println("Has sortit del programa");
		
		sc.close();
	}
}