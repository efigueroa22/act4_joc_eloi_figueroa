package exercicis_Strings_Wrappers_Llistes;

public class Ex5 {

	public static void main(String[] args) {
		String hola  = "Hola";
		
		//a
		StringBuilder cadena = new StringBuilder("Hola Caracola");
		System.out.println(cadena);
		
		//b
		System.out.println("Capacidad inicial: "+ cadena.capacity());
		System.out.println("Longitud inicial: "+ cadena.length());
		
		//c
		//cadena.replace(0, 4, "Hay");
		cadena.replace(cadena.indexOf("Hola"), hola.length(), "Hay");
		cadena.append("s");
		System.out.println(cadena);
		
		//d
		int i = 5000;
		cadena.insert(3,  " ");
		cadena.insert(4, i);
		System.out.println(cadena);
		
		//e
		cadena.append(" en el mar");
		System.out.println(cadena);
		
		//f
		String cadenaTemporal = cadena.substring(cadena.length()-4, cadena.length());
		System.out.println(cadenaTemporal);
		
		//g
		System.out.println("Capacidad final: "+ cadena.capacity());
		System.out.println("Longitud final: "+ cadena.length());
	}
}