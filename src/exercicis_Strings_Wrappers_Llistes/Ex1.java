package exercicis_Strings_Wrappers_Llistes;
import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String cadena = "hola";
		
		do {
			System.out.println("Cadena:");
			cadena = sc.nextLine();
		} while(cadena.equals(""));
			
		for(int i = cadena.length ()-1; i >= 0; i--) {
			System.out.println(cadena.charAt(i));
		}
		
		sc.close();
	}
}