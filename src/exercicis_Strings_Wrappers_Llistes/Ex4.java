package exercicis_Strings_Wrappers_Llistes;
import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String frase;
		
		System.out.println("Introdueix una frase");
		frase = sc.nextLine();
		
		String[] palabras = frase.split("\\s+");
		
		System.out.println("El numero de paraules es "+palabras.length);
		
		sc.close();
	}
}