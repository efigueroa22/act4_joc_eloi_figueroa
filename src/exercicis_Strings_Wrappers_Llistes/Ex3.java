package exercicis_Strings_Wrappers_Llistes;
import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		String cadena = "";
		Scanner sc = new Scanner(System.in);
		
		do {
			System.out.println("Cadena: ");
			cadena = sc.nextLine();
		}while(cadena.equals(""));
		
		StringBuilder cadenaSinEspacios = new StringBuilder(cadena);
		int i= 0;
		
		while (i < cadenaSinEspacios.length()-1) {
			if(cadenaSinEspacios.charAt(i) == ' ' && cadenaSinEspacios.charAt(i+1) == ' ') {
				cadenaSinEspacios.deleteCharAt(i+1);
		} else {
			i++;
		}
		
		sc.close();
		}
		System.out.println(cadenaSinEspacios);
	}
}