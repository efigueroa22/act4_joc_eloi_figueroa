package exercicis_Strings_Wrappers_Llistes;
import java.util.Scanner;

public class Ex7 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long num = 0;
		
		do {
			System.out.println("Introdueix un numero enter positiu");
			num = sc.nextLong();
		}while(num <= 0 || num >= Integer.MAX_VALUE);
		
		System.out.println("Numero correcte");
		
		sc.close();
	}
}