package m5;
import java.util.Random;
import java.util.Scanner;

public class Act4_Joc_Eloi_Figueroa {

	public static void main(String[] args) {
		int numAleatori, intents, opcio, darreraPartida;
		
		Scanner sc = new Scanner(System.in);
		Random random = new Random();
		
		darreraPartida = 0;
		
		do {
			System.out.println("Selecciona una opcio del menu");
			System.out.println("Opcio 1. Instruccions de com jugar");
			System.out.println("Opcio 2. Jugar");
			System.out.println("Opcio 3. Resultats de la darrera partida");
			System.out.println("Opcio 4. Sortir");
			
			opcio = sc.nextInt();
			
			if(opcio == 1) {
				System.out.println("Instruccions: ");
				System.out.println("S'ha generat un numero aleatori entre l'1 i el 10, i l'has d'endevinar.");
				System.out.println("Quan t'ho indiqui el xat, comença a introduir numeros.");
				System.out.println("Tens tres intents.\n");
			}
			else if(opcio == 2) {
				numAleatori = random.nextInt(10);
				System.out.println("El numero aleatori ha sigut escollit.");
				System.out.println("Endavant, pots provar els tres intents:");
				for (int i = 0; i < 3; i++) {
					intents = sc.nextInt();
					if(intents == numAleatori) {
						System.out.println("Has guanyat! El numero aleatori era el "+numAleatori+"\n");
						darreraPartida = 1;
						break;
					} 
					else if(i==2 && numAleatori != intents) {
						System.out.println("\n"+"Has perdut. S'han esgotat els tres intents i no has endivinat el numero:(\n");
						darreraPartida = 2;
					}
					else if(intents != numAleatori) {
						System.out.println("Has fallat. Introdueix un altre numero");
					}
				}
			}
			else if(opcio == 3) {
				if(darreraPartida == 1) {
					System.out.println("Has guanyat la darrera partida!\n");
				}
				else if(darreraPartida == 2) {
					System.out.println("Has perdut la darrera partida!\n");
				}
				else {
					System.out.println("No hi han dades registrades.\n");
				}
			}
		} while(opcio != 4);
		
		System.out.println("Has sortit");
		
		sc.close();
	}
}