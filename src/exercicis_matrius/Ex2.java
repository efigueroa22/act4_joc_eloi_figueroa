package exercicis_matrius;

public class Ex2 {

	public static void main(String[] args) {
		int[][] matriu1 = {{2,0,1},{3,0,0},{5,1,1}};
		int[][] matriu2 = {{1,0,1},{1,2,1},{1,1,0}};
		int[][] matriu3 = new int[3][3];
		
		for (int i = 0; i < matriu1.length; i++) {
			for (int j = 0; j < matriu2[i].length; j++) {
				for (int k = 0; k < matriu2[i].length; k++) {
					System.out.print(" ");
					System.out.print(matriu1[i][k]);
					System.out.print("x");
					System.out.print(matriu2[k][j]);
					matriu3[i][j] += matriu1[i][k]*matriu2[k][j];
				}
				System.out.print("=" + matriu3[i][j]);
			}
			System.out.println();
		}
	}
}