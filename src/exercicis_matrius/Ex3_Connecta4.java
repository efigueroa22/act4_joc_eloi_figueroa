package exercicis_matrius;
import java.util.Arrays;
import java.util.Scanner;

public class Ex3_Connecta4 {

	public static void main(String[] args) {	
		Scanner sc = new Scanner(System.in);
		String tirada;
		int columnaTirada;
		int filaTirada = -1;
		int jugadorActual = 1;
		int jugadorGuanyador = 0;
		int connecta4 = 0;
		int filaMinima, columnaMinima;
		
		char[][] taulell = new char[6][7];
		final char fitxa1 = 'X';
		final char fitxa2 = 'O';
		char fitxaActual;
		
		// inicialitzar taulell
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell[i].length; j++) {
				taulell[i][j] = '-';
			}
		}
		// pintar taulell
		for (int i = 0; i < taulell.length; i++) {
			System.out.println(Arrays.toString(taulell[i]));
		}
		
		// començar a jugar fins que l'usuari polsi "S" o hi hagi un guanyador
		do {
			connecta4 = 0;
			fitxaActual = (jugadorActual == 1) ? fitxa1 : fitxa2;
			System.out.println("Jugador: "+jugadorActual);
			System.out.print("Introdueix columna: ");
			tirada = sc.next();
			if (!tirada.equalsIgnoreCase("S")) {
				columnaTirada = Integer.valueOf(tirada)-1;
				
				// colocar ficha el mes avall possible
				for (int i = taulell.length-1; i >= 0 ; i--) {
					if (taulell[i][columnaTirada] == '-') {
						taulell[i][columnaTirada] = fitxaActual;
						filaTirada = i;
						break;
					}
				}
				
				// comprovar si ha fet connecta 4 en fila tirada
				for (int j = 0; j < taulell[filaTirada].length; j++) {
					if (taulell[filaTirada][j] == fitxaActual) {
						connecta4++;
					} else {
						connecta4 = 0;
					}
					if(connecta4 == 4) {
						jugadorGuanyador = jugadorActual;
						break;
					}
				}
				
				// comprovar si ha fet connecta 4 en columna tirada
				for (int i = 0; i < taulell.length; i++) {
					if (taulell[i][columnaTirada] == fitxaActual) {
						connecta4++;
					} else {
						connecta4 = 0;
					}
					if(connecta4 == 4) {
						jugadorGuanyador = jugadorActual;
						break;
					}
				}
				
				// comprovar si ha fet connecta 4 en diagonal esquerra
				// primer busquem la fila i columna mínima
				// si la tirada es en filaTirada = columnaTirada --> filaMinima = columnaMinima = 0
				filaMinima = 0;
				columnaMinima = 0;
				
				if(filaTirada > columnaTirada) {
					filaMinima = filaTirada - columnaTirada;
					columnaMinima = 0;
				} else if(filaTirada < columnaTirada) {
					filaMinima = 0;
					columnaMinima = columnaTirada - filaTirada;
				}
				
				// miramos la diagonal
				connecta4 = 0;
				int fila = filaMinima;
				int columna = columnaMinima;
				
				while(fila < taulell.length-1 && columna < taulell[fila].length-1) {
					if (taulell[fila][columna] == fitxaActual) {
						connecta4++;
					} else {
						connecta4 = 0;
					}
					if(connecta4 == 4) {
						jugadorGuanyador = jugadorActual;
						break;
					}
					fila++;
					columna++;
				}
				
				// comprovar si ha fet connecta 4 en diagonal dreta
				// primer busquem la fila i columna mínima
				// si la tirada es en filaTirada = columnaTirada --> filaMinima = columnaMinima = 0
				filaMinima = 0;
				columnaMinima = 0;
				
				if(filaTirada > columnaTirada) {
					filaMinima = filaTirada - columnaTirada;
					columnaMinima = 0;
				} else if(filaTirada < columnaTirada) {
					filaMinima = 0;
					columnaMinima = columnaTirada - filaTirada;
				}
				
				// miramos la diagonal
				connecta4 = 0;
				int filaEsq = filaMinima;
				int columnaDreta = columnaMinima;
				
				while(fila > taulell.length-1 && columna < taulell[fila].length-1) {
					if (taulell[fila][columna] == fitxaActual) {
						connecta4++;
					} else {
						connecta4 = 0;
					}
					if(connecta4 == 4) {
						jugadorGuanyador = jugadorActual;
						break;
					}
					fila--;
					columna--;
				}
				
				
			}
			
			// pintar el taulell amb les fitxes posades
			for (int i = 0; i < taulell.length; i++) {
				System.out.println(Arrays.toString(taulell[i]));
			}
			
			jugadorActual = (jugadorActual == 1) ? 2 : 1;
			
		} while(!tirada.equalsIgnoreCase("S") && jugadorGuanyador == 0);
		System.out.println("Jugador guanyador "+jugadorGuanyador);
		
		sc.close();
	}
}