package exercicis_ordinogrames_finals;
import java.util.Random;
import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		int num_Aleatori, num_Usuari;
		Random random;
		Scanner sc;
		
		random = new Random();
		sc = new Scanner(System.in);
		num_Aleatori = random.nextInt(100);
		
		do {
			System.out.println("Introdueix un numero");
			num_Usuari = sc.nextInt();
			
			if(num_Usuari < num_Aleatori){
				System.out.println("El numero introduit es mes petit que el numero aleatori");
			}
			else if(num_Usuari > num_Aleatori){
				System.out.println("El numero introduit es mes gran que el numero aleatori");
			}
			else {
				System.out.println("S'ha encertat el nombre aleatori");
			}
		} while(num_Usuari != num_Aleatori);
		
		sc.close();
	}
}