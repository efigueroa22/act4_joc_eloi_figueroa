package exercicis_ordinogrames_finals;
import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		int limit, num, suma;
		Scanner sc;
		
		sc = new Scanner(System.in);
		suma = 0;
		
		
		
		do {
			System.out.println("Introdueix un valor limit positiu");
			limit = sc.nextInt();
		} while(limit<=0);
		
		do {
			System.out.println("Introdueix un numero");
			num = sc.nextInt();
			
			suma = suma + num;
		} while(suma <= limit);
		
		
		
		System.out.println("La suma dels numeros ha superat el valor limit");
		
		sc.close();
	}
}