package exercicis_ordinogrames_finals;
import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		int num_menu, num1, num2;
		Scanner sc;
		
		sc= new Scanner(System.in);
		
		do {
			System.out.println("0. Sortir");
			System.out.println("1. Suma");
			System.out.println("2. Resta");
			System.out.println("3. Multiplicacio");
			System.out.println("4. Divisio");
			
			num_menu = sc.nextInt();
			
			if(num_menu == 1){
				System.out.println("Introdueix num1");
				num1 = sc.nextInt();
				System.out.println("Introdueix num2");
				num2 = sc.nextInt();
				System.out.println(num1+num2);
			}
			else if(num_menu == 2) {
				System.out.println("Introdueix num1");
				num1 = sc.nextInt();
				System.out.println("Introdueix num2");
				num2 = sc.nextInt();
				System.out.println(num1-num2);
			}
			else if(num_menu == 3) {
				System.out.println("Introdueix num1");
				num1 = sc.nextInt();
				System.out.println("Introdueix num2");
				num2 = sc.nextInt();
				System.out.println(num1*num2);
			}
			else if(num_menu == 4) {
				System.out.println("Introdueix num1");
				num1 = sc.nextInt();
				System.out.println("Introdueix num2");
				num2 = sc.nextInt();
				System.out.println((double)num1/num2);
			}
		} while(num_menu != 0);
		
		sc.close();
	}
}