package arrays;

public class dimension1 {

	public static void main(String[] args) {
		// Opció 1
		
		String[] dies1 = new String[7];
		
		dies1[0] = "Dilluns";
		dies1[1] = "Dimarts";
		dies1[2] = "Dimecres";
		dies1[3] = "Dijous";
		dies1[4] = "Divendres";
		dies1[5] = "Dissabte";
		dies1[6] = "Diumenge";
		
		// Opció 2
		
		String[] dies2 = {"Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge"};
		
		for (int i = 0; i < dies2.length; i++) {
			System.out.println(dies2[i]);
		}
		
		for (String dia : dies2) {
			System.out.println(dia);
		}
	}
}