package arrays;
import java.util.Scanner;

public class dimension3 {

	public static void main(String[] args) {
		int portal, planta, puerta;
		
		Scanner sc = new Scanner(System.in);
		
		boolean[][][] pisosVenuts = new boolean[3][4][3];
		
		//pedimos el piso vendido
		
		System.out.println("¿Que piso has vendido?");
		System.out.println("Portal:");
		portal = sc.nextInt();
		System.out.println("Planta:");
		planta = sc.nextInt();
		System.out.println("Puerta:");
		puerta = sc.nextInt();
		
		//marcamos piso como vendido
		
		pisosVenuts[portal][planta][puerta] = true;
		
		//mostrar todos los pisos vendidos
		
		for (int i = 0; i < pisosVenuts.length; i++) {
			for (int j = 0; j < pisosVenuts[i].length; j++) {
				for (int k = 0; k < pisosVenuts[i][j].length; k++) {
					System.out.print("PORTAL: "+ i + " PLANTA: " + j + " PUERTA: "+ k);
					System.out.println(" --> "+ pisosVenuts[i][j][k]);
				}
			}
		}
		
		sc.close();
	}
}