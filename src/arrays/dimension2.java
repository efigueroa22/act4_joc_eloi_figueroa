package arrays;

public class dimension2 {

	public static void main(String[] args) {
		//OPCIO 1
		
		int[][] numeros1 = new int[3][5];
		numeros1[0][0] = 1;
		numeros1[0][1] = 2;
		numeros1[0][2] = 3;
		numeros1[0][3] = 4;
		numeros1[0][4] = 5;
		
		numeros1[1][0] = 2;
		numeros1[1][1] = 4;
		numeros1[1][2] = 6;
		numeros1[1][3] = 7;
		numeros1[1][4] = 10;
		
		numeros1[2][0] = 3;
		numeros1[2][1] = 6;
		numeros1[2][2] = 9;
		numeros1[2][3] = 12;
		numeros1[2][4] = 15;
		
		for (int i = 0; i < numeros1.length; i++) {
			for (int j = 0; j < numeros1[i].length; j++) {
				System.out.println(numeros1[i][j]);
			}
		}
		
		//OPCIO 2
		
		int[][] numeros2 = {{1,2,3,4,5},{2,4,6,8,10},{3,6,9,12,15}};
		
		for (int i = 0; i < numeros2.length; i++) {
			for (int j = 0; j < numeros2[i].length; j++) {
				System.out.println(numeros2[i][j]);
			}
		}
	}
}