package examen_uf1_EFD;
import java.util.Scanner;

public class AscendentsAbella_EFD {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		//int generacioFinal, num1, num2, suma;
		int generacioFinal;
		long num1, num2, suma;	//podríem tenir problemes amb el tipus int si els valors de les variables superen un número molt elevat.

		do{
			System.out.print("Introdueix el número de generacions a calcular (mínim 1): ");
			generacioFinal = sc.nextInt();
		}while(generacioFinal<1);

		//Calculem a partir de la generació d'una abella mascle 
		num1=0;
		num2=1;
		suma = 1;
		for(int generacio=1;generacio<=generacioFinal;generacio++){
			suma = num1 + num2;
			num1 = num2;
			num2 = suma;
			System.out.println("Generació: "+generacio+" Ancestres: "+num1); //he afegit aquesta línea per mostrar per pantalla el que demana l'exercici
		}
		
		sc.close();
	}
}