package examen_uf1_EFD;
import java.util.Scanner;

public class Ex3a_EFD {

	public static void main(String[] args) {
		int H, M, S, auxH, auxM, auxS;
		
		auxH = 0;
		auxM = 0;
		auxS = 0;
		
		Scanner sc = new Scanner(System.in);	
		
		System.out.println("Introdueix les hores:");
		H = sc.nextInt();
		System.out.println("Introdueix els minuts:");
		M = sc.nextInt();
		System.out.println("Introdueix els segons:");
		S = sc.nextInt();
		
		S++;
		
		if(S >= 60) {
			auxM = S/60;
			auxS = S%60;
		}
		if(M >= 60) {
			auxH = M/60;
			auxM = auxM+M%60;
		}
		if(H >= 24){
			auxH = auxH+H/24;
		}
		
		System.out.println(auxH+" "+auxM+" "+auxS);
		sc.close();
	}
}