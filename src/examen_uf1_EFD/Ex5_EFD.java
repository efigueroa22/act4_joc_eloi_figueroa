package examen_uf1_EFD;
import java.util.Scanner;

public class Ex5_EFD {

	public static void main(String[] args) {
		int uPaquet, numMaxCaixa, uRestant;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix el numero d'unitats que volem empaquetar:");
		uPaquet = sc.nextInt();
		
		System.out.println("Introdueix el numero d'unitats que hi caben a cada caixa:");
		numMaxCaixa = sc.nextInt();
		
		for (int i = 1; i <= uPaquet/numMaxCaixa; i++) {
			System.out.println("Caixa "+i);
			for (int j = 1; j <= numMaxCaixa; j++) {
				System.out.println("Caixa "+i+" Unitat "+j);
			}
			System.out.println("...");
		}
		uRestant = uPaquet%numMaxCaixa;
		System.out.println("Sobren "+uRestant+" unitats");
		
		sc.close();
	}
}