package examen_uf1_EFD;
import java.util.Scanner;

public class Ex2_EFD {

	public static void main(String[] args) {
		int numtaules, num;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix el numero de taules:");
		numtaules = sc.nextInt();
		
		for (int i = 1; i <= numtaules; i++) {
			System.out.println("Taula del "+i);
			for (int j = 1; j <= 10; j++) {
				num = i*j;
				System.out.println(i+"*"+j+"="+num);
			}
		}
		sc.close();
	}
}