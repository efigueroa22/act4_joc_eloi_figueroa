package examen_uf1_EFD;
import java.util.Scanner;

public class SumarDeXEnX_AmbErrors_EFD {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);	//inicialitzar Scanner
		//int numeroFinal, n;
		int numeroFinal, n, suma;	//declarar les tres variables que utilitzarà el programa
		suma = 0;	//posar valor 0 a la variable suma

		do{		//Demanar un numero final per teclat fins que s'introdueixi un numero major o igual a 10
			System.out.print("Introdueix el número final (mínim 10): ");	//es mostra per pantalla el missatge
			numeroFinal = sc.nextInt();		//es deana numeroFinal per teclat
		//}while(numeroFinal==10);
		}while(numeroFinal<10);		//condicio del bucle
		
		do{		//demanar un valor de quant en quant vols sumar per teclat fins que s'introdueixi un valor major o igual a 1
			System.out.print("Introdueix de quant en quant vols sumar (mínim 1): ");	//es mostra per pantalla el missatge
			n = sc.nextInt();		//es demana n per teclat
		//}while(n<0);
		}while(n<1);	//condicio del bucle

		//for (int i = 0; i <= 100; i++) {
		for (int i = 0; i < numeroFinal/n; i++) {		//s'inizialitza un bucle amb una variable i = 0, i amb la condicio de que mentres i sigui menor a numeroFinal/n, i suma 1
			//System.out.println(i);
			suma = suma + n;	//la variable suma es igual a el valor de suma mes el valor de n
			System.out.println(suma);	//es mostra per pantalla el valor de suma
		}
		sc.close();		//es tanca l'Scanner
	}
}