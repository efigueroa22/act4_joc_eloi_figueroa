package exercicis_basics_II;

public class Ex5_NombresPrimersOptimitzat {

	public static void main(String[] args) {
		
		boolean bPrimer;
		
		for (int num = 1; num <= 100; num = num+2) {
			bPrimer = true;
			
			for (int i = 3; i < num; i = i+2) {
				if (num % i == 0) {
					bPrimer = false;
					break;
				}
			}
			
			if(bPrimer==true) {
				System.out.println(num);
			}
		}
	}
}