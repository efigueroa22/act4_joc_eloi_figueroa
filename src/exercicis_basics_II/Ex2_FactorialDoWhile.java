package exercicis_basics_II;
import java.util.Scanner;

public class Ex2_FactorialDoWhile {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num_entrada;
		int contador = 1;
		long factorial = 1;
		
		System.out.println("Introdueix un numero: ");
		num_entrada = sc.nextInt();
		
		do {
			factorial = factorial*contador;
			contador++;
		} while (contador<=num_entrada);
		
		System.out.println("El factorial de "+ num_entrada +" es "+factorial);
		sc.close();
	}
}