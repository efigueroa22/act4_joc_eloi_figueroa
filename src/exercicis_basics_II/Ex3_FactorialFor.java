package exercicis_basics_II;
import java.util.Scanner;

public class Ex3_FactorialFor {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num_entrada;
		long factorial = 1;
		
		System.out.println("Introdueix un numero: ");
		num_entrada = sc.nextInt();
		
		for (int contador = 1; contador <= num_entrada; contador++) {
			factorial = factorial*contador;
		}
		
		System.out.println("El factorial de "+ num_entrada +" es "+factorial);
		sc.close();
	}
}