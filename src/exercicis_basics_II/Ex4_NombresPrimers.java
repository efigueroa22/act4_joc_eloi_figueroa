package exercicis_basics_II;

public class Ex4_NombresPrimers {

	public static void main(String[] args) {
		
		boolean primer;
		
		for (int num = 2; num <= 100; num++) {
			primer = true;
			
			for (int i = 2; i < num; i++) {
				if (num % i == 0) {
					primer = false;
					break;
				}
			}
			
			if(primer==true) {
				System.out.println(num);
			}
		}
	}
}