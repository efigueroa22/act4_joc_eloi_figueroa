package exercicis_basics_II;
import java.util.Scanner;

public class Ex1_FactorialWhile {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num_entrada;
		int contador = 1;
		long factorial = 1;
		
		System.out.println("Introdueix un numero: ");
		num_entrada = sc.nextInt();
		
		while (contador<=num_entrada) {
			factorial = factorial*contador;
			contador++;
		}
		
		System.out.println("El factorial de "+ num_entrada +" es "+factorial);
		sc.close();
	}
}