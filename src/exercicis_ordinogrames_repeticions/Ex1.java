package exercicis_ordinogrames_repeticions;

public class Ex1 {

	public static void main(String[] args) {
		int sumapares = 0;
		int sumaimpares = 0;
		
		for (int i = 1; i <= 100; i++) {
			if (i % 2 == 0) {
				sumapares = sumapares+i;
			}
			else {
				sumaimpares = sumaimpares+i;
			}
		}
		System.out.println("Suma pares="+ sumapares);
		System.out.println("Suma impares="+ sumaimpares);
	}
}