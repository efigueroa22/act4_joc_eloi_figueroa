package exercicis_ordinogrames_repeticions;
import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int num;
		
		do {
			System.out.println("Introdueix un numero");
			num = sc.nextInt();
		} while(num <= 0 );
			
		sc.close();
	}
}