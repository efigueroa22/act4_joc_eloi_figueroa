package exercicis_ordinogrames_repeticions;
import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int num1;
		int num2;
		int suma;
		
		System.out.println("Introdueix el num1");
		num1 = sc.nextInt();
		System.out.println("Introdueix el num2");
		num2 = sc.nextInt();
		
		suma = 0;
		
		while(num1 <= num2){
			suma = suma+num1;
			num1++;
		}
		System.out.println(suma);
		
		sc.close();
	}
}