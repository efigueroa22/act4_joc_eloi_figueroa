package exercicis_ordinogrames_repeticions;
import java.util.Scanner;

public class Ex5 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int nota;
		int sumanotas = 0;
		int numnotas = 0;
		int insuficient = 0, suficient = 0, be = 0, notable = 0, excelent = 0;
		int mitjana = 0;
		int contador = 0;
		
		do {
			System.out.println("Introdueix una nota");
			nota = sc.nextInt();
			contador++;
			mitjana = (mitjana + nota) / contador;
			
			if(nota >= 0 && nota <= 10) {
				sumanotas = sumanotas + nota;
				numnotas++;
				
				if(nota >= 0 && nota < 5) {
					insuficient++;
				}
				else if(nota >= 5 && nota < 6) {
					suficient++;
				}
				else if(nota >= 6 && nota < 7) {
					be++;;
				}
				else if(nota >= 7 && nota < 9) {
					notable++;
				}
				else{
					excelent++;
				}
			}
			
		} while(nota != -1);
		
		System.out.println("La mitjana aritmetica es "+ (double)(sumanotas/numnotas));
		System.out.println("Insuficients = "+insuficient);
		System.out.println("Suficients = "+suficient);
		System.out.println("Be = "+be);
		System.out.println("Notables = "+notable);
		System.out.println("Excelents = "+excelent);
		
		sc.close();
	}
}