package exercicis_ordinogrames_repeticions;
import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int num;
		
		
		
		do {
			System.out.println("Introdueix un numero");
			num = sc.nextInt();
			
			if(num != 0) {
				if (num > 0){
					System.out.println("Numero positiu");
				}
				else if(num < 0) {
					System.out.println("Numero negatiu");
				}
				
				if(num%2 == 0) {
					System.out.println("Numero parell");
				}
				
				if(num%4 == 0) {
					System.out.println("Numero multiple de 4");
				}
			}
		} while(num != 0);
		
		sc.close();
	}
}