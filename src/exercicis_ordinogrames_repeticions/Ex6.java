package exercicis_ordinogrames_repeticions;
import java.util.Scanner;

public class Ex6 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int num1, num2, resto;
		System.out.println("Introdueix el numero 1");
		num1 = sc.nextInt();
		System.out.println("Introdueix el numero 2");
		num2 = sc.nextInt();
		
		do {
			resto = num1 % num2;
			
			if(resto == 0) {
				System.out.println("MCD = "+num2);
			} 
			else {
			num1 = num2;
			num2 = resto;
			}
			
		}while(resto != 0);
		
		sc.close();
	}
}