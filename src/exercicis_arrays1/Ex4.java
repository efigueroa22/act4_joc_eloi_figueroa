package exercicis_arrays1;
import java.util.Random;
import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		int limit, petit, major;
		
		Scanner sc = new Scanner(System.in);
		Random random = new Random();
		
		System.out.println("Introdueix un limit");
		limit = sc.nextInt();
		
		int[] numeros = new int[limit];
		
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = random.nextInt(100);
			System.out.println(numeros[i]);
		}
		petit = numeros[0];
		major = 0;
		
		for (int i = 0; i < numeros.length; i++) {
			if(numeros[i] < petit) {
				petit = numeros[i];
			}
			if(numeros[i] > major) {
				major = numeros[i];
			}
		}
		System.out.println("petit: "+petit);
		System.out.println("major: "+major);
		
		sc.close();
	}
}