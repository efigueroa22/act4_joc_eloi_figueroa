package exercicis_arrays1;

import java.util.Arrays;

public class Ex6 {

	public static void main(String[] args) {
		int aux;
		int[] nums = {47, 34, 97, 3, 14};
		
		for (int i = 0; i < nums.length-1; i++) {
			for (int j = 0; j < nums.length-i-1; j++) {
				if(nums[j+1] < nums[j]){
					aux = nums[j+1];
					nums[j+1] = nums[j];
					nums[j] = aux;
				}
			}
		}
		System.out.println(Arrays.toString(nums));
	}
}