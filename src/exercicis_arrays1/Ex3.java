package exercicis_arrays1;
import java.util.Random;
import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		int limit;
		
		Scanner sc = new Scanner(System.in);
		Random random = new Random();
		
		System.out.println("Introdueix un limit");
		limit = sc.nextInt();
		
		int[] numeros = new int[limit];
		
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = random.nextInt(20);
			System.out.println(numeros[i]);
		}
		
		for (int i = 0; i < numeros.length; i++) {
			for (int j = i+1; j < numeros.length; j++) {
				if(numeros[i] == numeros[j]) {
					System.out.println(numeros[i] + " repetit");
				}
			}
		}
		sc.close();
	}
}