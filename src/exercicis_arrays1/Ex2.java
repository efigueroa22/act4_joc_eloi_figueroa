package exercicis_arrays1;
import java.util.Random;
import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		int limit;
		Scanner sc = new Scanner(System.in);
		Random random = new Random();
		
		System.out.println("Introdueix un limit");
		limit = sc.nextInt();
		
		int[] numeros = new int[limit];
		
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = random.nextInt(100);
			System.out.println(numeros[i]);
		}
		sc.close();
	}
}