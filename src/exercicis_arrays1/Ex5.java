package exercicis_arrays1;

import java.util.Arrays;

public class Ex5 {

	public static void main(String[] args) {
		int minim, aux;
		int[] nums = {47, 34, 97, 3, 14};
		
		for (int i = 0; i < nums.length-1; i++) {
			minim = i;
			for (int j = i+1; j < nums.length; j++) {
				if(nums[j] < nums[minim]) {
					minim = j;
				}
			}
			aux = nums[i];
			nums[i] = nums[minim];
			nums[minim] = aux;
		}
		System.out.println(Arrays.toString(nums));
	}
}