package exercicis_arrays1;
import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		int opcio, suma, multiplicacio;
		Scanner sc = new Scanner(System.in);
		
		int[] numeros = new int[5];
		
		suma = 0;
		multiplicacio = 1;
		
		do {
			System.out.println("Selecciona una opcio del menu:");
			System.out.println("1. Suma");
			System.out.println("2. Multiplicacio");
			opcio = sc.nextInt();
			
			if(opcio == 1) {
				for (int i = 0; i < 5; i++) {
					System.out.println("Introdueix un num:");
					numeros [i] = sc.nextInt();
					suma = suma + numeros[i];
				}
				System.out.println(suma);
			}
			else if(opcio == 2) {
				for (int i = 0; i < 5; i++) {
					System.out.println("Introdueix un num:");
					numeros [i] = sc.nextInt();
					multiplicacio = multiplicacio * numeros[i];
				}
				System.out.println(multiplicacio);
			}
		} while(opcio != 1 && opcio != 2);
		
		sc.close();
	}
}