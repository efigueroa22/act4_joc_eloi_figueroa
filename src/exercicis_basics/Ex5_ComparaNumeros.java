package exercicis_basics;

import java.util.Scanner;

public class Ex5_ComparaNumeros {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String int_num1 = "Introdueix el num1:";
		System.out.println(int_num1);
		int num1 = sc.nextInt();
		
		String int_num2 = "Introdueix el num2:";
		System.out.println(int_num2);
		int num2 = sc.nextInt();
		
		String iguals = "Els dos numeros son iguals";
		String num1_maj = "El num1 es major que el num2";
		String num2_maj = "El num2 es major que el num1";
		
		if (num1==num2) {
			System.out.println(iguals);
		} else if (num1>num2){
			System.out.println(num1_maj);
		}else {
			System.out.println(num2_maj);
		}
		
		sc.close();
	}
}