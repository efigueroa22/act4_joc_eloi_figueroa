package exercicis_basics;

import java.util.Scanner;

public class Ex6_DiaSemana {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String demana_num = "Introdueix un numero:";
		System.out.println(demana_num);
		int num1 = sc.nextInt();
		
		String dilluns = "Dilluns";
		String dimarts = "Dimarts";
		String dimecres = "Dimecres";
		String dijous = "Dijous";
		String divendres = "Divendres";
		String dissabte = "Dissabte";
		String diumenge = "Diumenge";
		String incorrecte = "Dia incorrecte";
		
		if (num1 == 1) {
			System.out.println(dilluns);
		} else if(num1 == 2) {
			System.out.println(dimarts);
		} else if(num1 == 3) {
			System.out.println(dimecres);
		} else if(num1 == 4) {
			System.out.println(dijous);
		} else if(num1 == 5) {
			System.out.println(divendres);
		} else if(num1 == 6) {
			System.out.println(dissabte);
		} else if(num1 == 7) {
			System.out.println(diumenge);
		} else if(num1 > 1) {
			System.out.println(incorrecte);
		} else {
			System.out.println(incorrecte);
		}
		
		sc.close();
	}
}