package exercicis_basics;

import java.util.Scanner;

public class Ex7_NumDiesMesAny {

	public static void main(String[] args) {
		int numDiesMesAny = 10;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Any:");
		int any = sc.nextInt();
		System.out.println("Mes:");
		int mes = sc.nextInt();
		
		switch (mes) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			numDiesMesAny = 31;
			break;
			
		case 4:
		case 6:
		case 9:
		case 11:
			numDiesMesAny = 30;
			break;
			
		case 2:
			if ((any%4 == 0 && any%100 != 0) || (any%400 == 0)){
				numDiesMesAny = 29;
			} else {
				numDiesMesAny = 28;
			}
			break;
			
		default:
			System.out.println("Mes incorrecte");
			break;
		}
		System.out.println("El mes " + mes + " de l'any " + any + " te " + numDiesMesAny + " dies");
		
		sc.close();
	}
}