package exercicis_basics;

public class Ex2 {

	public static void main(String[] args) {
		String recepta_truita = "Ingredients: Dos ous, una mica de sal i oli d'oliva.\n" 
		+ "Elaboracio:\n" 
		+ "1. Batre els ous a un recipient.\n" 
		+ "2. Ficar una mica de sal i remenar.\n" 
		+ "3. Posar un raig d'oli en una paella i posarla al foc.\n" 
		+ "4. Quan estigui calent l'oli, afegir els ous batuts a la paella, repartint-los per la superficie.\n" 
		+ "5. Tapar la paella i deixar a foc lent cinc minuts.\n" 
		+ "6. Donar la volta a la truita amb ajuda d'un plat un minut mes.\n" 
		+ "7. Treure la truita de la paella, i a menjar!";
		System.out.println(recepta_truita);
	}
}