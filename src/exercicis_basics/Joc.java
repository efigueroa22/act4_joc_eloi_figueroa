package exercicis_basics;
import java.util.Random;
import java.util.Scanner;

public class Joc {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Escriu el tu nom i cognom");
		String nom = sc.next();
		System.out.println("Benvingut " + nom);
	
		boolean end = false;
		while (end == false) {
			System.out.println("--------------------");
			System.out.println("Tria una opció");
			System.out.println("1. Canviar nom");
			System.out.println("2. jugar");
			System.out.println("3. sortir");
			int opcio = sc.nextInt();
	
			switch (opcio) {
			case 1: 
				System.out.println("Escriu el tu nom");
				nom = sc.next();
				System.out.println("Hola " + nom);
				break;
			case 2:
				System.out.println("Tria a un numero entre el 1 y el 10");
				int aposta = sc.nextInt();
				Random rnd = new Random();
				int numeroGuanyador= rnd.nextInt(1, 10);
				if (aposta == numeroGuanyador) {
					System.out.println("Has guanyat!, En hora bona "+nom);
				}else {
					System.out.println("Has perdut, el nombre correcte es: "+numeroGuanyador);
				}	
				break;
			case 3:
				System.out.println("Adeu!");
				end = true;
				break;
			default:
				System.out.println("L'opció triada es incorrecte!");
			}
		}
		sc.close();
	}
}