package exercicis_basics;

import java.util.Scanner;

public class Ex4_Multiplica2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String desc_num1 = "Introdueix el num1:";
		System.out.println(desc_num1);
		int num1 = sc.nextInt();
		
		String desc_num2 = "Introdueix el num2:";
		System.out.println(desc_num2);
		int num2 = sc.nextInt();
		
		int multiplicacio = num1*num2;
		String missatge = "La multiplicacio de num1 x num2 es ";
		System.out.println(missatge+multiplicacio);
		sc.close();
	}
}