package jo_el;
import java.util.Scanner;

public class Pruebas {

	public static void main(String[] args) {
		int num1, num2, minim, maxim;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix num1:");
		num1 = sc.nextInt();
		System.out.println("Introdueix num2:");
		num2 = sc.nextInt();
		
		if(num1 < num2) {
			minim = num1;
			maxim = num2;
		}
		else {
			minim = num2;
			maxim = num1;
		}
		
		for (int i = minim+1; i <= maxim; i++) {
			if(i%2 == 0) {
				System.out.println(i+" es numero par");
			}
		}
		
		sc.close();
	}
}