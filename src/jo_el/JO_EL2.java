package jo_el;
import java.util.Scanner;

public class JO_EL2 {

	public static void main(String[] args) {
		int casos, fila, contador, piramide;
		Scanner sc = new Scanner(System.in);
		
		casos = sc.nextInt();
		contador = 0;
		piramide = 1;
		
		do {
			fila = sc.nextInt();
			contador++;
			piramide = contador*casos;
			System.out.println(piramide);
		} while(contador < casos);
		
		sc.close();
	}
}