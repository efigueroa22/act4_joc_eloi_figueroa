package bucles;

public class BucleFor {

	public static void main(String[] args) {
		int diesAny = 365;
		
		for (int diaActual = 1; diaActual <= diesAny; diaActual++) {
			System.out.println(diaActual);
		}
	}
}