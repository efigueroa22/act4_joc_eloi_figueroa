package bucles;
import java.util.Scanner;

public class BucleDoWhile {

	public static void main(String[] args) {
		
		int num_entrada = 0;
		int contador = 1;
		int factorial = 1;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix un numero: ");
		num_entrada = sc.nextInt();
		
		do {
			factorial = factorial*contador;
			contador++;
		} while (contador<=num_entrada);
		
		System.out.println("El factorial de "+ num_entrada +" es "+factorial);
		
		sc.close();
	}
}