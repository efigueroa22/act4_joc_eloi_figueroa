package bucles;

public class ExempleBucle {

	public static void main(String[] args) {
		
		int diesAny = 365;
		int diaActual = 1;
		
		while (diaActual <= diesAny) {
			System.out.println(diaActual);
			diaActual++;
		}
	}
}