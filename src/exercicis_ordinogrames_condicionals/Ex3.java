package exercicis_ordinogrames_condicionals;
import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix la quantitat");
		int quantitat = sc.nextInt();
		
		System.out.println("Introdueix el preu");
		double preu = sc.nextInt();
		
		double importe = quantitat*preu;
		System.out.println("L'import es de "+importe);
		
		if(importe>500 && importe<=1000){
			importe = (importe*0.80);
			
			System.out.println("S'aplica un descompte del 20% i l'import final es de "+importe);
		}
		else if(importe>1000) {
			importe = (importe*0.60);
			
			System.out.println("S'aplica un descompte del 40% i l'import final es de "+importe);
		}
		
		sc.close();
	}
}