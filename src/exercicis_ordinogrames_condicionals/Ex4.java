package exercicis_ordinogrames_condicionals;
import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nom1;
		String nom2;
		String nom3;
		int edat1;
		int edat2;
		int edat3;
		
		System.out.println("Introdueix el nom de la primera persona");
		nom1 = sc.next();
		System.out.println("Introdueix l'edat de la primera persona");
		edat1 = sc.nextInt();
		
		System.out.println("Introdueix el nom de la segona persona");
		nom2 = sc.next();
		System.out.println("Introdueix l'edat de la segona persona");
		edat2 = sc.nextInt();
		
		System.out.println("Introdueix el nom de la tercera persona");
		nom3 = sc.next();
		System.out.println("Introdueix l'edat de la tercera persona");
		edat3 = sc.nextInt();
		
		if(edat1 > edat2 && edat1 > edat3) {
			System.out.println("La persona mes gran es "+nom1+" amb una edat de "+edat1);
			if(edat2 > edat3) {
				System.out.println("La segona persona mes gran es "+nom2+" amb una edat de "+edat2);
				System.out.println("La tercera persona mes gran es "+nom3+" amb una edat de "+edat3);
			}
			else if(edat2 < edat3){
				System.out.println("La segona persona mes gran es "+nom3+" amb una edat de "+edat3);
				System.out.println("La tercera persona mes gran es "+nom2+" amb una edat de "+edat2);
			}
		}
		else if(edat2 > edat1 && edat2 > edat3) {
			System.out.println("La persona mes gran es "+nom2+" amb una edat de "+edat2);
			if(edat1 > edat3) {
				System.out.println("La segona persona mes gran es "+nom1+" amb una edat de "+edat1);
				System.out.println("La tercera persona mes gran es "+nom3+" amb una edat de "+edat3);
			}
			else if(edat1 < edat3){
				System.out.println("La segona persona mes gran es "+nom3+" amb una edat de "+edat3);
				System.out.println("La tercera persona mes gran es "+nom1+" amb una edat de "+edat1);
			}
		}
		else if(edat3 > edat1 && edat3 > edat2) {
			System.out.println("La persona mes gran es "+nom3+" amb una edat de "+edat3);
			if(edat1 > edat2) {
				System.out.println("La segona persona mes gran es "+nom1+" amb una edat de "+edat1);
				System.out.println("La tercera persona mes gran es "+nom2+" amb una edat de "+edat2);
			}
			else if(edat1 < edat2){
				System.out.println("La segona persona mes gran es "+nom2+" amb una edat de "+edat2);
				System.out.println("La tercera persona mes gran es "+nom1+" amb una edat de "+edat1);
			}
		}
		sc.close();
	}
}