package exercicis_ordinogrames_condicionals;
import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix un numero");
		int num = sc.nextInt();
		
		if(num%2==0 && num%3==0){
			System.out.println("Es multiple de dos y de tres");
		}
		else if(num%2==0) {
			System.out.println("Es multiple de dos");
		}
		else if(num%3==0) {
			System.out.println("Es multiple de tres");
		}
		else {
			System.out.println("No es multiple ni de dos ni de tres");
		}
		
		sc.close();
	}
}