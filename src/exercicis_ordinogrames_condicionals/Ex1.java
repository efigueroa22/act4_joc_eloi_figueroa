package exercicis_ordinogrames_condicionals;
import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix un numero");
		int num = sc.nextInt();
		
		if(num>10){
			System.out.println("Es mes gran que 10");
		}
		else if(num==10) {
			System.out.println("Es igual que 10");
		}
		else {
			System.out.println("Es mes petit que 10");
		}
		
		sc.close();
	}
}